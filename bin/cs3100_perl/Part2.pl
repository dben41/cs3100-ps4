#open FILE, "../files/capitalize.txt";

# The loop condition stores one line at a time into $_
while (<STDIN>)
{
    # Print the input with every Java-style single-line comment 
    # (beginning with // and continuing to the end of the line) 
    # removed.  The newline character should not be removed.    
	s/(\/\/.*)//g;
	print $_;
}
