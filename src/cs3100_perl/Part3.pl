# The loop condition stores one line at a time into $_
$count = 0;
while (<STDIN>)
{
     # Count the number of times a telephone number formatted as (xxx) xxx-xxxx 
     # appears in the input and write this count to the output.  The space after 
     # the area code is optional.  Each phone number must be preceded with the 
     # beginning of a line or white space; each phone number must be followed 
     # by the end of a line or white space.
     # 
     # Tests:
     # (901)234-2323 is a valid number. (801) 567-9876 also valid
     #(912)923-2321 isn't valid, and notvalid(901)-334-9938 and
     # (234) 123-1232isn't valid either. In total, I expect 2.
    
    while (m/(\s|^)\(\d{3,}\)( ||)\d{3,}-\d{4,}(\s|$)/g)
	{
		$count++;
	}
}
print $count;
	
