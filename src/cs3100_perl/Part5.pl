#open FILE, "../files/capitalize.txt";

# The loop condition stores one line at a time into $_
while (<STDIN>)
{
    # Replace each occurrence in $_ of the pattern
    # with the value of the expression.
    #  Option g = global (replace all patterns)
    #  Option e = evaluate (evaluate the replacement)    
	s/("([^"]*?||.||\\"|\\\\)*"+)/$2$1/g;
	s/(<((".*")|([^<>])*)*>)//g;
	
	
	print $_;
}

