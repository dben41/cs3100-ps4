#Print the input with every Social Security number (three digits, a hyphen, two digits, a hyphen, four digits; e.g. 123-45-6789) replaced with ###-##-####.  Each SSN must be preceded with the beginning of a line or white space; each SSN must be followed by the end of a line or white space.
#
#Pass
# 123-12-1232
#My social is 123-12-1232
#My friend's social is 123-23-1232 and mine is 123-23-1232 
#
#Fail
#f232-23-2322
#My social is 123-12-2322.
#My friend's social is 9122-12-2322.
while (<STDIN>)
{  
	s/(\s\d{3,}-\d{2,}-\d{4,}\s)/ ###-##-#### /ig;
	s/(^\d{3,}-\d{2,}-\d{4,}\s)/###-##-#### /ig;
	print $_;
}
