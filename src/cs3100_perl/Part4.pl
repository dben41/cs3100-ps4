
#Print every properly-formatted string that appears in the input.  A properly-formatted 
#string begins with a quote mark (") and ends with the next quote mark that appears on 
#the same line.  Inside a string, a backslash followed by any other character should be 
#treated as a two-character escape sequence.  A backslashed quote mark does not terminate 
#a string.  Thus, "one", "two\"", and "three\\" are all properly-fomatted strings.
#
# Test:
#  "one", "two\"", and "three\\", "Sweet." the nice "boy\" ate cheese.
while (<STDIN>)
{
    # Loop over matches in $_
    #  Option g = global (all matches)
    while (m/("([^"]*?||.||\\"|\\\\)*"+)/g)
    {
        # Print the first and second capture groups
        print "$1\n";
    }
}