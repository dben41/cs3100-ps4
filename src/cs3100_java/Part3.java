package cs3100_java;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part3
{
    /**
     * Count the number of times a telephone number formatted as (xxx) xxx-xxxx 
     * appears in the input and write this count to the output.  The space after 
     * the area code is optional.  Each phone number must be preceded with the 
     * beginning of a line or white space; each phone number must be followed 
     * by the end of a line or white space.
     * 
     * Tests:
     * (901)234-2323 is a valid number. (801) 567-9876 also valid
     *(912)923-2321 isn't valid, and notvalid(901)-334-9938 and
     * (234) 123-1232isn't valid either. In total, I expect 2.
     * 
     */
    public static void main (String[] args) throws IOException
    {
        // Create a Scanner to read from the test file
    	Scanner s = new Scanner(System.in);
        // This regex matches words.
        Pattern p = Pattern.compile("(\\s|^)[(]\\d{3}[)]( ?)\\d{3}-\\d{4}(\\s|$)");
        
     // Use this variable to accumulate the answer
        int count = 0;
        
        while (s.hasNextLine())
        {
        	// Match on the current line
            Matcher m = p.matcher(s.nextLine());
            
            // Replace each matched word with its Pig Latin equivalent
            while (m.find())
            {
                count++;
            }
        }    
        // Display the answer
        System.out.println(count);
    }
}
