package cs3100_java;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part5
{
    /**
     * Print the input with every HTML tag removed.  For our purposes, an HTML
     * tag starts with a < and ends with the next unquoted >, which need not be
     *  on the same line.  No unquoted < characters may appear inside a tag. 
     *   All quote marks (") that appear inside of a tag must be part of properly 
     *   formatted strings (see above) that begin and end within the tag.  A < or > 
     *   character that appears inside such a properly formatted string is treated as quoted.
     *   
     *   TEST: <p>Built and "my favorite tag <hello> " Maintained by <a href="http://www.darylbennett.net">Daryl Bennett</a></p>
     */
    public static void main (String[] args) throws IOException
    {
        // Create a Scanner to read from the test file
    	Scanner s = new Scanner(System.in);
        // This regex matches words.
    	Pattern p = Pattern.compile("(<((\".*\")|([^<>])*)*>)" + "|" + "\"([^\"]*?|.|\\\\\"|\\\\\\\\)*\""); 
    	
    	String document = "";
        // Pull the entire document into a string
    	while (s.hasNextLine())
		{
    		document += s.nextLine() + "\n";
		}
    	
		// Replace every substring that matches the pattern with
		// the string "COMMENT"
		Matcher m = p.matcher(document);
		 // The modified text will be accumulated into the StringBuffer
        StringBuffer sb = new StringBuffer();
        
        // Replace each matched word with its Pig Latin equivalent
        while (m.find())
        {
            // This part deals with matches to the first part of the pattern
            if (m.group(1) != null)
            {
                m.appendReplacement(sb, "");
            }
            // This part deals with matches to the second part of the pattern
            else{
                //Do nuthin'
            }
        }
        // Copy over any remaining text into the StringBuffer, then print it out.
        m.appendTail(sb);
        System.out.println(sb);
    }
}
