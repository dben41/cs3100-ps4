package cs3100_java;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part2
{
    /**
     * Print the input with every Java-style single-line comment 
     * (beginning with // and continuing to the end of the line) removed.  
     * The newline character should not be removed.
     */
    public static void main (String[] args) throws IOException
    {
        // Create a Scanner to read from the test file
    	Scanner s = new Scanner(System.in);
        // This regex matches words.
        //Pattern p = Pattern.compile("\\w+");
        Pattern p = Pattern.compile("(//.*)", Pattern.DOTALL);

        // Pull the entire document into a string
        
        while (s.hasNextLine())
        {
        	String document = "";
        	document += s.nextLine() + "\n";
            // Match on the entire document
            Matcher m = p.matcher(document);

            // The modified text will be accumulated into the StringBuffer
            StringBuffer sb = new StringBuffer();

            // Replace every substring that matches the pattern with
            // a capitalized version.
            while (m.find())
            {
            	String ss = m.group();
                m.appendReplacement(sb, "");
            }

            // Copy over any remaining text into the StringBuffer, then print it out.
            m.appendTail(sb);
            String temp = sb.toString();
            System.out.println(sb);
        }

       
    }
}
