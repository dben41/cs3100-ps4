package cs3100_java;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1
{
    /**
     * 1. Print the input with every Social Security number (three digits, a hyphen, two digits, a hyphen, four digits; e.g. 123-45-6789) replaced with ###-##-####.  Each SSN must be preceded with the beginning of a line or white space; each SSN must be followed by the end of a line or white space.
     *  Pass
	 *		123-12-1232
	 *		My social is 123-12-1232
	 *		My friend's social is 123-23-1232 and mine is 123-23-1232
     *
	 *	Fail
	 *		f232-23-2322
	 *		My social is 123-12-2322.
	 *		My friend's social is 9122-12-2322.
     */
    public static void main (String[] args) 
    {
        // Create a Scanner to read from the test file
    	Scanner s = new Scanner(System.in);
        // This regex matches words.
        Pattern p = Pattern.compile("(?:\\s|^)\\d{3}-\\d{2}-\\d{4}(?:\\s|$)");

        // Pull the entire document into a string       
        while (s.hasNextLine())
        {
            // Match on the entire document
            Matcher m = p.matcher(s.nextLine());
            
            // The modified text will be accumulated into the StringBuffer
            StringBuffer sb = new StringBuffer();

            // Replace every substring that matches the pattern with
            // a capitalized version.
            while (m.find())
            {
            	if( m.group().charAt(0) == ' ')
            		m.appendReplacement(sb, " ###-##-#### ");
            	else
            		m.appendReplacement(sb, "###-##-#### ");
            }
            
            // Copy over any remaining text into the StringBuffer, then print it out.
            m.appendTail(sb);
            System.out.println(sb);
        }


    }
}
