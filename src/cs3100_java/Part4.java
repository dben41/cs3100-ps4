package cs3100_java;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part4
{
    /**
     * Print every properly-formatted string that appears in the input.  A properly-formatted 
     * string begins with a quote mark (") and ends with the next quote mark that appears on 
     * the same line.  Inside a string, a backslash followed by any other character should be 
     * treated as a two-character escape sequence.  A backslashed quote mark does not terminate 
     * a string.  Thus, "one", "two\"", and "three\\" are all properly-fomatted strings.
     * 
     * Tests:
     * "one", "two\"", and "three\\", "Sweet." the nice "boy\" ate cheese.
     */
    public static void main (String[] args) throws IOException
    {
        // Create a Scanner to read from the test file
    	Scanner s = new Scanner(System.in);
        // This regex matches words.

    	Pattern p = Pattern.compile("\"([^\"]*?|.|\\\\\"|\\\\\\\\)*\"+"); 
        // Pull the entire document into a string
        
        while (s.hasNextLine())
        {
        	// For each line, print out the match and the domain
		    // part of the match
			Matcher m = p.matcher(s.nextLine());
			while (m.find())
			{
				System.out.println(m.group());
			}
        }

       
    }
}
